#!/usr/bin/perl -w
use common::sense;

my $var = MyCompare->new();

$var->compare( from => 'test1_10mb.txt', with => 'test2_10mb.txt', diff => 'test3_10mb.txt' );

package MyCompare;

sub new {
    my $invoc = shift;
    my $class = ref($invoc) || $invoc;
    my $self  = {};
    bless $self, $class;
    return $self;
}

sub compare {
    my $self   = shift;
    my $params = {@_};
    open( FROM, "<", $params->{from} ) || die "$!";
    open( WITH, "<", $params->{with} ) || die "$!";
    open( DIFF, ">", $params->{diff} ) || die "$!";
    while ( my $line1 = <FROM> ) {
        chomp($line1);
        my $found = 0;
        while ( my $line2 = <WITH> ) {
            chomp($line2);
            $found = 1 if $line1 eq $line2;
            last if ($found);
        }
        seek( WITH, 0, 0 );
        say DIFF $line1 unless $found;

    }

    close(FROM);
    close(WITH);
    close(DIFF);

}

1;


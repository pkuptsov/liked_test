#!/usr/bin/perl -w
use common::sense;

my $file = 'big_file.txt';

open(FILE, '>>', $file) or die $!;
my @V = ("A".."Z","z".."a",0..9);
my $i = 0;

while ($i<= 1000000) {
    my $j = int(rand($#V));
    print FILE  $V[$j];
    $i++;
    print FILE "\n" if ($i%30) == 0;
}
close(FILE);




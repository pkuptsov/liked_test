#!/usr/bin/perl -w
use common::sense;
use MongoDB;

my $var = MyCompare->new();

$var->compare( from => 'test1_10mb.txt', with => 'test2_10mb.txt', diff => 'test3_10mb.txt' );

package MyCompare;

sub new {
    my $invoc = shift;
    my $class = ref($invoc) || $invoc;
    my $self  = {};
    bless $self, $class;
    return $self;
}
#

sub compare {
    my $self   = shift;
    my $params = {@_};
    my $mongo  = MongoDB::MongoClient->new();
    my $db     = $mongo->get_database('liked');
    my $col    = $db->get_collection('with');

    _init_collections( $col, $params->{with} );

    open( FROM, "<", $params->{from} ) || die "$!";
    open( DIFF, ">", $params->{diff} ) || die "$!";

    while ( my $line1 = <FROM> ) {
        chomp($line1);
        my $found = $col->find_one( { line => $line1 } );
        say DIFF $line1 unless $found;

    }

    close(FROM);
    close(DIFF);
    $col->drop;
}
#
sub _init_collections {
    my ( $col, $file ) = @_;
    my $no_index = 1;
    open( WITH, "<", $file ) || die "$!";
    while ( my $row = <WITH> ) {
        chomp($row);
        $col->insert_one( { line => $row } );
        if ($no_index) {
            $col->ensure_index( { line => 1 } );
            $no_index = 0;
        }

    }
    close(WITH);
}
1;

